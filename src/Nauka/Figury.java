package Nauka;


class Boki{
	
	int a;
	int b;
	
	Boki(int a, int b){
	this.a = a;
	this.b = b;
	
	}
	
	void pokazFigure() {
		System.out.println("Bok a: " + a);
		System.out.println("Bok b: " + b);
	}
}	
	
class Trojkaty extends Boki{
	
	int c;
	
	Trojkaty(int a, int b, int c){
		super(a, b);
		this.c = c;
	
	}
	
	void pokazFigure() {
		super.pokazFigure();
		System.out.println("Bok c: " + c);
	}
	
}
	


public class Figury {
	
	public static void main(String[]args){
		
		Trojkaty t1 = new Trojkaty(9, 3, 5);
		t1.pokazFigure();
		
	}

}
