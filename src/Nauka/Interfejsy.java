package Nauka;

// INTERFACE TO TAKI JAKBY WZORZEC

interface Produkts {
// w interfejsie wszystkie pola s� statyczne i finalne, zawsze musimy wykona� inicjalizacj�	
// w interfejsach wszystkie metody s� abstrakcyjne i nie musimy tego pisa�
	final static String nazwa = "Kurs Access";
	double cena = 40;
	
//final static - nie musi by� dodawane poniewa� dla interface dodawane jest domy�lnie
	
	void pokazNazwe();
	void pokazCene();

}

class Courses implements Produkts{

// ka�da klasa kt�ra implamentuje interfejs musi implementoa� wszystkie metody interfejsu
// w przeciwie�stwie do klas abstrakcyjnych	gdzie mo�emy tworzy� metody abstrakcyjne lub nie
// w przypadku interfejsu wszystkie metody s� abstrakcyjne	
	
	public void pokazNazwe() {
		System.out.println(nazwa);
	}


	public void pokazCene() {
	// TODO Auto-generated method stub
		System.out.println(cena);
	}
}

public class Interfejsy {
	
	public static void main (String[]args) {
		
		Courses kurs1 = new Courses();
		kurs1.pokazNazwe();
		kurs1.pokazCene();
		
	}

}
