package Nauka;

abstract class Produkty{
// klasa abstrakcyjna - nie posiada w�asnych instancji/nie mo�e posiada� �adnych obiekt�w
	String nazwa;
	
	Produkty(String nazwa){
		this.nazwa = nazwa;
	}
	
	abstract void pokazInformacje();
// metoda abstrakcyjna - tworz�c j� w klasie bazowej za ka�dym razem musimy stworzy� jej interpretacje w klasie pochodnej

}

class Kursy extends Produkty {
	
	double cena;
	int rozdzialy;
	
	Kursy (String nazwa, double cena, int rozdzialy){
		super(nazwa);
		this.cena = cena;
		this.rozdzialy = rozdzialy;
	}
	void pokazInformacje() {
		
		System.out.println(nazwa);
		System.out.println(cena);
		System.out.println(rozdzialy);
// implementacja metody abstrakcyjnej w klasie pochodnej		
	}
}

class Ksiazki extends Produkty{
	
	int strony;
	
	Ksiazki (String nazwa, int strony) {
		super(nazwa); 
		this.strony = strony;
	}
// musimy doda� interpretacje abstrakcyjnej metody z klasy bazowej
// metoda ta w ka�dej klasie mo�e wykonywa� co innego 
	void pokazInformacje() {
		System.out.println(nazwa);
		System.out.println(strony);
	}
	
}

public class Abstrakcyjne {
	
	public static void main(String[] args) {
		
//		Produkty p1 = new Produkty("Produkt 1");
//		nie mo�emy przypisa� obiekt�w do klasy abstrakcyjnej
		
		Kursy kurs1 = new Kursy ("Kurs Excel", 42.5, 3);
		kurs1.pokazInformacje();
		
		Ksiazki ksiazka1 = new Ksiazki("Nazwa ksi��ki", 438);
		ksiazka1.pokazInformacje();
	}

}
