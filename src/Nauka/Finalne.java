package Nauka;

class Firma{
	
}

//final class Dzial extends Firma{
// po dodaniu final jest b��d w klasie Sekcja poniewa� nie mo�na dziedziczy� z klasy finalnej
class Dzial extends Firma{
	
}

class Sekcja extends Dzial{
//finalne pole MUSI by� zainicjalizowane, MUSI mie� warto�� pocz�tkow�, tej warto�ci nie mo�na p�iej zmieni�

	final String nazwa = "Kurs Java";
	
//	Sekcja(String nazwa){
//		this.nazwa = nazwa;
//	}
// pole mo�my zainicjalizowa� r�wnie� w konstruktorze, nie musi by� od razu
// musimy jednak wtedy usun�� inicjalizacj� z g�ry i doda� j� na dole przy wywo�aniu konstruktora	
}

public class Finalne {
	
	public static void main(String[] args) {
		
//		Sekcja sekcja1 = new Sekcja("Kurs Java");
		
		
	}

}
