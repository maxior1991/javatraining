package Nauka;

class Drzewa {
	
	String nazwa = "Buk";
	
	class Krzaki{
		// klasa wewn�trzna mo�e uzyskiwa� dost�p do p� w klasie zewn�trznej
		
		int wysokosc;
		String wielkosc;
		
		void pokazPrzyrode() {
			System.out.println(nazwa);
		}
	}
}

public class Wewnetrzne {
	
	public static void main (String[]args) {
		
		Drzewa drzewo1 = new Drzewa();
		Drzewa.Krzaki krzak1 = drzewo1.new Krzaki();
		krzak1.pokazPrzyrode();
		
		
		
		// odwo�anie do klasy wewn�trznej
		
		
	}

}
